<?php

DEFINE('DS', DIRECTORY_SEPARATOR); 
require "app".DS."Scraper".DS."ScraperAutoload.php";

use KScraper\Core\Scraper;
use KScraper\Tools\ScraperTest;

$scraper = new Scraper;
new ScraperTest($scraper);

?>