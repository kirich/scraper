# Scraper

A simple Scraper using a modern PHP OOP app structure with PSR-4 Compliant autoloader.

You can use the Scraper to fetch images or links from any target url.

Use like this:
```
$scraper->setScrapeMode("images");
$scraper->setTargetSite("https://www.google.com/search?q=zebras&safe=off");
$scraper->scrape();
print_r($scraper->getResults());
```

If you do not have PHP DOMDocument installed you can run:

```
sudo apt-get install php-dom
```
OR
```
yum install php-xml
```


The default Scraper mode is images

Set up the scraper and then open a terminal window and call

```
php index.php
```