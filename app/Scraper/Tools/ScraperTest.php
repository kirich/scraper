<?php

namespace KScraper\Tools;

use KScraper\Core\Scraper;

class ScraperTest {
    public function __construct(Scraper $scraper)
    {
        echo "Starting test...".PHP_EOL;
        
        $scraper->setScrapeMode("images");
        $scraper->setTargetSite("https://www.google.com/search?q=zebras&safe=off");
        $scraper->scrape();
        print_r($scraper->getResults());

    }
}
