<?php

namespace KScraper\Core;

class Scraper {
    
    private $target_class;
    private $target_site;
    private $scrape_mode = "images";
    private $modes = [
        "images"=>"img",
        "links"=>"a",
    ];
    private $collection = [];

    public function __construct()
    {
        echo "New Scraper created".PHP_EOL;
    }

    public function setTargetClass($target_class){
        $this->target_class = $target_class;
    }

    public function setTargetSite($target_site){
        $this->target_site = $target_site;
    }

    public function getTargetSite($target_site){
        return $this->target_site;
    }

    public function setScrapeMode($mode){
        if(array_key_exists($mode, $this->modes)){
            $this->scrape_mode = $mode;
        }
        else{
            echo "Invalid mode '".$mode."'";
            exit();
        }
    }

    public function scrape(){
        if (!isset($this->target_site)){ echo "Please set a TARGET SITE with setTargetSite('some_url')"; return ;}
        
        try{
            $the_site = $this->target_site;
            $the_tag = "div";
            $the_class = "gravatar-wrapper-32";
        
            $html = @file_get_contents($the_site);
            libxml_use_internal_errors(true);
            if(!empty($html)){
                $dom = new \DOMDocument();
                $dom->loadHTML($html);
                $xpath = new \DOMXPath($dom);

                $xpath_query_path = isset($this->target_class) ? '//[contains(@class,"'.$this->target_class.'")]/'.$this->modes[$this->scrape_mode] : '//'.$this->modes[$this->scrape_mode];
                //$results = [];

                if (!isset($this->collection[$this->scrape_mode])) { 
                    $this->collection[$this->scrape_mode] = []; 
                }

                foreach ($xpath->query($xpath_query_path) as $item) {
                    $target_src =  $this->scrape_mode == "images" ? $item->getAttribute('src') : $item->getAttribute('href');
                    !in_array($this->rel2abs($target_src), $this->collection[$this->scrape_mode]) ? array_push( $this->collection[$this->scrape_mode], $this->rel2abs($target_src) ) : false;
                    echo $target_src;
                }

                echo ".";
            }
        }
        catch(\Exception $e){
            echo "-";
        }

    }

    public function getResults(){
        return $this->collection;
    }

    private function rel2abs($url){
        $len = strlen($this->target_site);
        $is_foreign = (strpos($url, 'http') !== false) ? true : false;
        if ( strncmp($url, $this->target_site, $len) !== 0 && !$is_foreign )  {
            return $this->target_site.$url;
        }
        else{
            return $url;
        }
    }
       
}

